package com.solon.open.controller;

import com.alibaba.dashscope.exception.NoApiKeyException;
import com.alibaba.dashscope.exception.UploadFileException;
import com.solon.open.model.ImageModel;
import com.solon.open.service.ImageService;
import io.minio.errors.MinioException;
import org.dromara.milvus.plus.model.vo.MilvusResp;
import org.dromara.milvus.plus.model.vo.MilvusResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.core.handle.UploadedFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
public class ImageController {

    @Inject
    private ImageService imageService;

    /**
     * 添加图片
     * @param file
     * @return
     * @throws NoApiKeyException
     * @throws UploadFileException
     * @throws IOException
     */
    @Mapping("/image/upload")
    @Post
    public MilvusResp upload(Long imageId,String imageName,UploadedFile file) throws NoApiKeyException, UploadFileException, IOException, MinioException, NoSuchAlgorithmException, InvalidKeyException {
        return imageService.add(imageId,imageName,file);
    }

    /**
     * 搜图
     * @param
     * @return
     * @throws NoApiKeyException
     * @throws UploadFileException
     */
    @Mapping("/image/search")
    @Post
    public MilvusResp<List<MilvusResult<ImageModel>>> question(UploadedFile file) throws NoApiKeyException, UploadFileException, MinioException, IOException, NoSuchAlgorithmException, InvalidKeyException {
        return imageService.search(file);
    }
}