package com.solon.open.model;

import io.milvus.v2.common.DataType;
import io.milvus.v2.common.IndexParam;
import lombok.Data;
import org.dromara.milvus.plus.annotation.ExtraParam;
import org.dromara.milvus.plus.annotation.MilvusCollection;
import org.dromara.milvus.plus.annotation.MilvusField;
import org.dromara.milvus.plus.annotation.MilvusIndex;

import java.util.List;

@Data
@MilvusCollection(name = "image_collection")
public class ImageModel {

    @MilvusField(
            name = "id", // 字段名称
            dataType = DataType.Int64, // 数据类型为64位整数
            isPrimaryKey = true, // 标记为主键
            autoID = false // 假设这个ID是自动生成的
    )
    private Long id; // 唯一标识符

    @MilvusField(
            name = "image_name",
            dataType = DataType.VarChar
    )
    private String imageName;

    @MilvusField(
            name = "question_vector", // 字段名称
            dataType = DataType.FloatVector, // 数据类型为浮点型向量
            dimension = 1536 // 向量维度
    )
    @MilvusIndex(
            indexType = IndexParam.IndexType.IVF_FLAT, // 使用IVF_FLAT索引类型
            metricType = IndexParam.MetricType.IP, // 使用L2距离度量类型
            indexName = "image_index", // 索引名称
            extraParams = { // 指定额外的索引参数
                    @ExtraParam(key = "nlist", value = "16384") // 例如，IVF的nlist参数
            }
    )
    private List<Float> imageVector; // 问题向量

}
