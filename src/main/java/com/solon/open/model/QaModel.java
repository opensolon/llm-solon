package com.solon.open.model;

import com.solon.open.entity.KeyWord;
import io.milvus.v2.common.DataType;
import io.milvus.v2.common.IndexParam;
import lombok.Data;
import org.dromara.milvus.plus.annotation.ExtraParam;
import org.dromara.milvus.plus.annotation.MilvusCollection;
import org.dromara.milvus.plus.annotation.MilvusField;
import org.dromara.milvus.plus.annotation.MilvusIndex;
import org.ttzero.excel.annotation.ExcelColumn;

import java.util.List;

@Data
@MilvusCollection(name = "qa_collection")
public class QaModel {

    @MilvusField(
            name = "id", // 字段名称
            dataType = DataType.Int64, // 数据类型为64位整数
            isPrimaryKey = true, // 标记为主键
            autoID = true // 假设这个ID是自动生成的
    )
    private Long id; // 唯一标识符

    @MilvusField(
            name = "question",
            dataType = DataType.VarChar
    )
    @ExcelColumn("问题")
    private String question;

    @MilvusField(
            name = "answer",
            dataType = DataType.VarChar
    )
    @ExcelColumn("回答")
    private String answer;

    @MilvusField(
            name = "keyword",
            dataType = DataType.JSON
    )
    private KeyWord keyword;

    @MilvusField(
            name = "question_vector", // 字段名称
            dataType = DataType.FloatVector, // 数据类型为浮点型向量
            dimension = 1536 // 向量维度
    )
    @MilvusIndex(
            indexType = IndexParam.IndexType.IVF_FLAT, // 使用IVF_FLAT索引类型
            metricType = IndexParam.MetricType.L2, // 使用L2距离度量类型
            indexName = "question_index", // 索引名称
            extraParams = { // 指定额外的索引参数
                    @ExtraParam(key = "nlist", value = "100") // 例如，IVF的nlist参数
            }
    )
    private List<Float> questionVector; // 问题向量

}
