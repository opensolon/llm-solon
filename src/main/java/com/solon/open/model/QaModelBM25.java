package com.solon.open.model;

import io.milvus.v2.common.DataType;
import lombok.Data;
import org.dromara.milvus.plus.annotation.AnalyzerParams;
import org.dromara.milvus.plus.annotation.MilvusCollection;
import org.dromara.milvus.plus.annotation.MilvusField;
import org.dromara.milvus.plus.model.AnalyzerType;
import org.ttzero.excel.annotation.ExcelColumn;

@Data
@MilvusCollection(name = "qa_collection_bm25")
public class QaModelBM25 {

    @MilvusField(
            name = "id",
            dataType = DataType.Int64,
            isPrimaryKey = true,
            autoID = true
    )
    private Long id;

    @MilvusField(
            name = "question",
            dataType = DataType.VarChar,
            enableMatch = true,
            enableAnalyzer = true,
            analyzerParams = @AnalyzerParams(
                    type= AnalyzerType.CHINESE
            )
//            analyzerParams = @AnalyzerParams(
//                    tokenizer= "standard",
//                    filter=@Filter(
//                            builtInFilters={
//                                    BuiltInFilterType.lowercase
//                            },
//                            customFilters = {
//                                  @CustomFilter(
//                                      type = "length",
//                                      max = 40
//                                  ),
//                                  @CustomFilter(
//                                        type = "stop",
//                                        stopWords = {"of","to"}
//                                )
//                            }
//                    )
//            )
    )
    @ExcelColumn("问题")
    private String question;

    @MilvusField(
            name = "answer",
            dataType = DataType.VarChar
    )
    @ExcelColumn("回答")
    private String answer;


}
