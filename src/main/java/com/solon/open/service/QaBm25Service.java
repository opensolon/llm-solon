package com.solon.open.service;

import com.solon.open.config.QaConst;
import com.solon.open.mapper.QaBm25MilvusMapper;
import com.solon.open.model.QaModelBM25;
import org.apache.commons.collections4.CollectionUtils;
import org.dromara.milvus.plus.model.vo.MilvusResp;
import org.dromara.milvus.plus.model.vo.MilvusResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.List;

@Component
public class QaBm25Service {
    @Inject
    QaBm25MilvusMapper qaBm25MilvusMapper;

    public void addQa(List<QaModelBM25> model){
        qaBm25MilvusMapper.insert(model);
    }
    public String getQa(String question){
        MilvusResp<List<MilvusResult<QaModelBM25>>> query = qaBm25MilvusMapper.
                 queryWrapper().textVector(QaModelBM25::getQuestion,question)
                .topK(1).query();
        List<MilvusResult<QaModelBM25>> data = query.getData();
        if(CollectionUtils.isNotEmpty(data)){
            MilvusResult<QaModelBM25> qaModelMilvusResult = data.get(0);
            return qaModelMilvusResult.getEntity().getAnswer();
        }
        return QaConst.DEFAULT_ANSWER;
    }
}
