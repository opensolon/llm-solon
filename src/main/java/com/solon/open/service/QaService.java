package com.solon.open.service;

import com.google.common.collect.Lists;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import com.hankcs.hanlp.tokenizer.NLPTokenizer;
import com.solon.open.config.QaConst;
import com.solon.open.entity.KeyWord;
import com.solon.open.mapper.QaMilvusMapper;
import com.solon.open.model.QaModel;
import org.apache.commons.collections4.CollectionUtils;
import org.dromara.milvus.plus.model.vo.MilvusResp;
import org.dromara.milvus.plus.model.vo.MilvusResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class QaService {
    @Inject
    QaMilvusMapper qaMilvusMapper;

    public void addQa(List<QaModel> model){
        for (QaModel qaModel : model) {
            List<String> keyword = keyword(qaModel.getQuestion());
            qaModel.setKeyword(new KeyWord(keyword));
        }
        qaMilvusMapper.insert(model);
    }
    public String getQa(String question,List<Float> v){
        List<String> keyword = keyword(question);
        MilvusResp<List<MilvusResult<QaModel>>> query = qaMilvusMapper.
                 queryWrapper().vector(v)
                .jsonContainsAny("keyword[\"kw\"]",keyword)
                .topK(1).query();
        List<MilvusResult<QaModel>> data = query.getData();
        if(CollectionUtils.isNotEmpty(data)){
            MilvusResult<QaModel> qaModelMilvusResult = data.get(0);
            if(qaModelMilvusResult.getDistance()<0.3){
                return qaModelMilvusResult.getEntity().getAnswer();
            }
        }
        return QaConst.DEFAULT_ANSWER;
    }
    private List<String> keyword(String inputStr) {
        ArrayList<String> natures = Lists.newArrayList("n","Ng","v");
        List<Term> segment = NLPTokenizer.segment(inputStr);
        List<String> keyword = segment.stream().filter(v -> natures.contains(v.nature.toString())).map(a -> a.word).collect(Collectors.toList());
        return keyword;
    }

    public static void main(String[] args) {
        String temp="议长堪布索朗丹陪出席第18届西藏青年大会的开幕式照片/议会秘书处西藏人民议会议长\n" +
                "堪布索朗丹培为首的议会常务委员于 8 月 27 日出席了由西藏青年协会主办的第 18 届四藏\n" +
                "青年大会的开幕式。第 18 届西藏青年大会的开幕式上西藏人民议会议长堪布索朗丹陪作为\n" +
                "首席嘉宾出席了会议，藏人行政中央安全部部长嘉日卓玛、印度-西藏友好协会（ITFA）前\n" +
                "主席阿杰-辛格-曼科蒂亚(Shri Ajay Singh Mankotia)和达然萨拉印度-西藏友好协会主席阿\n" +
                "吉特河(Shri Ajit Nehria)作为特别嘉宾出席了会议。该会议上还有西藏人民议会常务委员\n";
        Segment segment = HanLP.newSegment().enableAllNamedEntityRecognize(true);
        List<Term> seg = segment.seg(temp);
        System.out.printf("=====");
    }
}
