package com.solon.open.service;

import com.alibaba.dashscope.embeddings.*;
import com.alibaba.dashscope.exception.ApiException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.alibaba.dashscope.exception.UploadFileException;
import com.solon.open.config.DashscopeConfiguration;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EmbeddingService {
    @Inject
    private DashscopeConfiguration configuration;

    public MultiModalEmbeddingResult embedding(Integer type, List<String> data) throws ApiException, NoApiKeyException, UploadFileException {
        MultiModalEmbedding embedding = new MultiModalEmbedding();
        List<MultiModalEmbeddingItemBase> contents;
        switch (type){
            case 0:
                 contents = data.stream().map(v -> {
                    MultiModalEmbeddingItemText text = new MultiModalEmbeddingItemText(v);
                    return text;
                }).collect(Collectors.toList());

                break;
            case 1:
                 contents = data.stream().map(v -> {
                    MultiModalEmbeddingItemImage image = new MultiModalEmbeddingItemImage(v);
                    return image;
                }).collect(Collectors.toList());
                break;
            case 2:
                 contents = data.stream().map(v -> {
                    MultiModalEmbeddingItemAudio text = new MultiModalEmbeddingItemAudio(v);
                    return text;
                }).collect(Collectors.toList());
                break;
            default:
                return null;
        }
        MultiModalEmbeddingParam param =
                MultiModalEmbeddingParam.builder()
                        .model(MultiModalEmbedding.Models.MULTIMODAL_EMBEDDING_ONE_PEACE_V1)
                        .contents(contents)
                        .apiKey(configuration.getApiKey())
                        .build();
        MultiModalEmbeddingResult result = embedding.call(param);
        return result;
    }
}
