package com.solon.open.config;

import lombok.Data;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

/**
 * @author xgc
 **/
@Data
@Inject("${dashscope}")
@Configuration
public class DashscopeConfiguration {
    private String apiKey;
}