package com.solon.open.config;

import io.minio.MinioClient;
import lombok.Data;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

@Data
@Inject("${minio}")
@Configuration
public class MinioConfig {
 
    private String endpoint;
 
    private String accessKey;
 
    private String secretKey;

    private String bucketName;
 
    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey, secretKey)
                .build();
    }
}