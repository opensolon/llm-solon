package com.solon.open.entity;

import lombok.Data;

@Data
public class QaReq {
    private String question;
}
