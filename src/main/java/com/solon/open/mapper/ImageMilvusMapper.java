package com.solon.open.mapper;


import com.solon.open.model.ImageModel;
import org.dromara.solon.mapper.MilvusMapper;
import org.noear.solon.annotation.Component;

@Component
public class ImageMilvusMapper extends MilvusMapper<ImageModel> {

}
